<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@page isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Login Page</title>
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
	rel="stylesheet">



<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha1/css/bootstrap.min.css"
	integrity="sha384-r4NyP46KrjDleawBgD5tp8Y7UzmLA05oM1iAEQ17CSuDqnUK2+k9luXQOfXJCJ4I"
	crossorigin="anonymous">
<link rel="stylesheet" type="text/css"
	href=<c:url value="/resources/loginPage.css" />>
<script
	src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
	integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
	crossorigin="anonymous"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha1/js/bootstrap.min.js"
	integrity="sha384-oesi62hOLfzrys4LxRF63OJCXdXDipiYWBnvTl9Y9/TRlw5xlKIEHpNyvvDShgf/"
	crossorigin="anonymous"></script>
</head>

<style>
.container {
	margin: 100px;
}

.form-control {
	width: 300px;
}

.card-header {
	padding-top: 72px;
	background: -webkit-gradient(linear, left top, left bottom, from(#767afb),
		to(white));
}

.card-body {
	padding-top: 120px;
	padding-bottom: 120px;
}

.col-sm-6 {
	margin-left: 20px
}

.bulged {
	border: solid #6a87b2;
	border-radius: 5px;
}

.bulged:active {
	background-color: #d4eaff;
	box-shadow: 0 2px #d4eaff;
	transform: translateY(2px);
}

.card-footer {
	padding-bottom: 25px;
	height: 50px;
	background: -webkit-gradient(linear, left bottom, left top, from(#767afb),
		to(white));
}

.loginbtn {
	padding: 25px;
}

.formbody {
	padding: 40px;
	margin-top: 76px;
	margin-bottom: 80px;
}

fieldset {
	border: 1px solid #000;
}

legend {
	margin-top: -20px;
	width: 90px;
	background: white;
	color: cornflowerblue;
	font-family: fangsong;
}
</style>


<body>


	<form method=post action="processform">

		<!--container-->


		<div class="card text-center">

			<!-- Header -->
			<div class="card-header">
				<ul class="nav nav-pills card-header-pills">
					<li class="nav-item" class="nav-link active"
						style="color: rgb(63, 86, 143);"></li>
				</ul>
			</div>

			<!-- Form body -->
			<div class="card text-center">


				<div class="formbody">
					<fieldset>
						<legend>Login</legend>
						<div class="row align-items-center"
							style="justify-content: center;">


							<!-- if username and passwords are incorrect -->
							<%
								boolean isUser = (Boolean) request.getAttribute("incorrect");
								if (!isUser) {
							%>
							<p class="incorrect">Incorrect Username and Password</p>
							<%
								}
							%>


							<div class="col-auto">
								<label for="inputEmail" class="col-form-label"><b>Username:</b></label>
							</div>

							<div class="col-auto">
								<input type="text" minlength="5" maxlength="50" name="userid"
									id="inputEmail" class="form-control">
							</div>

						</div>

						<br>

						<div class="row align-items-center"
							style="justify-content: center;">

							<div class="col-auto">
								<label for="inputPassword" class="col-form-label"><b>Password:</b></label>
							</div>

							<div class="col-auto">
								<input type="password" name="password" id="inputPassword"
									class="form-control" minlength="5" maxlength="50">
							</div>

						</div>
						<div class="loginbtn">
							<button type="submit" onclick="localS()" class="bulged">
								<b>Login</b>
							</button>
						</div>
					</fieldset>
				</div>
			</div>


			<!-- Footer -->
			<div class="card-footer"></div>

		</div>

	</form>
</body>
<script>
	function localS() {
		localStorage.removeItem("name");
		let username = document.getElementById("inputEmail").value;
		localStorage.setItem("name", username);
	}
</script>

</html>